<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp;

class ApiController extends Controller
{

    public function startBusinessProcess(Request $request)
    {
        $client = new GuzzleHttp\Client();
        $res = $client->post(env('MODULE_URI').'startbp', [
            GuzzleHttp\RequestOptions::JSON => [
                'bp' => $request->bpCode,
                'login' => $request->login,
                'lang_test' => true,
                'internal_id' => 'ФЛ-234',
                'password' => $request->password,
            ]
        ]);

        return json_decode($res->getBody(),true);
    }

    public function endBusinessProcess(Request $request)
    {
        $client = new GuzzleHttp\Client();
        $res = $client->post(env('MODULE_URI').'endbp', [
            GuzzleHttp\RequestOptions::JSON => [
                'bp' => $request->bpCode,
                'login' => $request->login,
                'id' => $request->id,
                'password' => $request->password,
            ]
        ]);

        return $res->getStatusCode();
    }

    public function endTask(Request $request)
    {
        $client = new GuzzleHttp\Client();
        $res = $client->post(env('MODULE_URI').'endtask', [
            GuzzleHttp\RequestOptions::JSON => [
                'bp' => $request->bpCode,
                'login' => $request->login,
                'id' => $request->id,
                'point' => $request->point,
                'password' => $request->password,
            ]
        ]);

        return json_decode($res->getBody(),true);
    }

    public function getPhoto(Request $request)
    {
        $client = new GuzzleHttp\Client();
        $res = $client->post(env('MODULE_URI').'getphoto', [
            GuzzleHttp\RequestOptions::JSON => [
                'bp' => $request->bpCode,
                'login' => $request->login,
                'id' => $request->id,
                'password' => $request->password,
            ]
        ]);

        return json_decode($res->getBody(),true);
    }
}


