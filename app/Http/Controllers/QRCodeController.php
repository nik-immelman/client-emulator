<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\QrCodeService;
use Illuminate\Http\Request;


class QRCodeController extends Controller
{
    /**
     * @param   Request $request
     */
    public function create(Request $request)
    {
        $qrService = new QrCodeService();
        $qrPrepareData = $qrService->prepareDataToQr($request->except('_token'));
        $qrString = $qrService->fillQrString($qrPrepareData);
        return '<h3>'.$qrString.'</h3>'.'<br />'.$qrService->paintQrCode($qrString);
        // I couldn't do it like this ->
/*        return [
            'qrImg' => $qrService->paintQrCode($qrString),
            'qrString' => $qrString
        ];*/
    }
}
