<?php


namespace App\Services;


use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class QrCodeService
{
    const SEPARATOR = "|";


    /**
     * Заполняет строку QR-кода из данных в data для документов направлений
     * @param $data
     * @return string
     */
    public function fillQrString($data)
    {
        $result = '';

        foreach ($data as $key => $value) {
            $result .= $this->addQrCodeParam($value);
        }
        return substr($result,0,-1);
    }

    /**
     * Добавляет значение параметра в строку QR-кода.
     * @param $value string значение до разделителя.
     * @return string
     */
    public function addQrCodeParam($value)
    {
        return $value . self::SEPARATOR;
    }

    /**
     * Выводит qrCode на экран
     * @param string $qrString
     */
    public function paintQrCode(string $qrString)
    {
        return QrCode::encoding('UTF-8')->size(300)->generate($qrString);
    }

    public function prepareDataToQr($data)
    {
        $data['birthday'] = !empty($data['birthday']) ? date('d.m.Y', strtotime($data['birthday'])) : null;
        $data['document_issued'] = !empty($data['document_issued']) ? date('d.m.Y', strtotime($data['document_issued'])) : null;
        $dul = $data['document_series'].','.$data['document_number'].','.$data['document_issued'];
        unset($data['document_series'], $data['document_number'],$data['document_issued']);
        array_splice($data, 7, 0,  $dul); //to follow the order, as in the documentation
        unset($data['login'], $data['password'], $data['bpCode']);

        return $data;
    }
}
