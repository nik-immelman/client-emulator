<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Partner emulator</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            input {
                margin: 10px;
            }
            .result_form {
                margin-top: 20px;
            }
        </style>
        <script type="text/javascript" src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js?ver=1.6.4'></script>
        <script>
        $( document ).ready(function() {
            let id = document.getElementById('id');
            let bpCode = document.getElementById('bpCode');
            let login = document.getElementById('login');
            let password = document.getElementById('password');
            $("#send_form").submit(function (e) {
                e.preventDefault();
                var form_data = $(this).serialize();
                $.ajax({
                    type: "POST",
                    url: "/qrcode",
                    data: form_data,
                    success: function (data) {
                        let pc = document.getElementById('result_form');
                        pc.innerHTML = data;
                    },

                });
            });
            $("#startbp").click(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "/startbp",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "bp": bpCode.value,
                        "login": login.value,
                        "password": password.value,
                    },
                    success: function (data) {
                        id.value = data.id;
                    }
                });
            });
            $("#endbp").click(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "/endbp",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id":id.value,
                        "bp": bpCode.value,
                        "login": login.value,
                        "password": password.value,
                    },
                    success: function (data) {
                        console.log(data);
                    }
                });
            });
            $("#getphoto").click(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "/getphoto",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id":id.value,
                        "bp": bpCode.value,
                        "login": login.value,
                        "password": password.value,

                    },
                    success: function (data) {
                        let image = document.getElementById('image');
                        image.innerHTML="<h3 >"+data.PpoComment+"</h3>"+"<img src='data:image/png;base64, "+data.Photo+"'" +
                            "width='385px'>";
                    }
                });
            });
            $("#medicine").click(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "/endTask",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id":id.value,
                        "point":"Медицина",
                        "bp": bpCode.value,
                        "login": login.value,
                        "password": password.value,
                    },
                    success: function (data) {
                        console.log(data);
                    }
                });
            });
            $("#testing").click(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "/endTask",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id":id.value,
                        "point":"Тестирование",
                        "bp": bpCode.value,
                        "login": login.value,
                        "password": password.value,
                    },
                    success: function (data) {
                        console.log(data);
                    }
                });
            });
        });

        </script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div style="display: flex;">
                <form id="send_form" name="send_form" class="send_form" method="POST" action="{{ route('qrcode.create') }}">
                    @csrf
                    <div class="form-group">
                        <label for="surname">Фамилия</label>
                        <input type="text" class="form-control" id="surname" name="surname" placeholder="Фамилия">
                    </div>
                    <div class="form-group">
                        <label for="name">Имя</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Имя">
                    </div>
                    <div class="form-group">
                        <label for="middle_name">Отчетсво</label>
                        <input type="text" class="form-control" id="middle_name" name="middle_name" placeholder="Отчество">
                    </div>
                    <div class="form-group">
                        <label for="gender">Пол</label>
                        <select class="form-control" id="gender" name="gender">
                            <option>М</option>
                            <option>Ж</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="birthday">Дата рождения</label>
                        <input type="date" class="form-control" id="birthday" name="birthday" placeholder="Дата рождения">
                    </div>
                    <div class="form-group">
                        <label for="country_birth">Страна рождения</label>
                        <input type="text" class="form-control" id="country_birth" name="country_birth" placeholder="Страна рождения">
                    </div>
                    <div class="form-group">
                        <label for="address_registration">Адрес регистрации</label>
                        <input type="text" class="form-control" id="address_registration" name="address_registration" placeholder="Адрес регистрации">
                    </div>
                    <div class="form-group">
                        <label for="document_series">Серия паспорта</label>
                        <input type="text" class="form-control" id="document_series" name="document_series" placeholder="Серия паспорта">
                    </div>
                    <div class="form-group">
                        <label for="document_number">Номер паспорта</label>
                        <input type="text" class="form-control" id="document_number" name="document_number" placeholder="Номер паспорта">
                    </div>
                    <div class="form-group">
                        <label for="document_issued">Дата выдачи паспорта</label>
                        <input type="date" class="form-control" id="document_issued" name="document_issued" placeholder="Дата выдачи паспорта">
                    </div>
                    <div class="form-group">
                        <label for="country_residence">Страна проживания</label>
                        <input type="text" class="form-control" id="country_residence" name="country_residence" placeholder="Страна проживания">
                    </div>
                    <div class="form-group">
                        <label for="nationality">Национальность</label>
                        <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Национальность">
                    </div>
                    <div style="display: flex;">
                        <div class="form-group">
                            <label for="id">id</label>
                            <input type="text" class="form-control" id="id" name="id" placeholder="id">
                        </div>
                        <button id="startbp" name="startbp" class="btn btn-primary mb-2" style="margin: 10px;">startbp</button>
                    </div>
                    <div style="display: flex">
                        <div class="form-group">
                            <label for="login">Логин</label>
                            <input type="text" class="form-control" id="login" name="login" placeholder="login">
                        </div>
                        <div class="form-group">
                            <label for="password">Пароль</label>
                            <input type="text" class="form-control" id="password" name="password" placeholder="password">
                        </div>
                        <div class="form-group">
                            <label for="bpCode">bpCode</label>
                            <input type="text" class="form-control" id="bpCode" name="bpCode" placeholder="bpCode">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Сгенерировать qr</button>
                    <button id="endbp" name="endbp" class="btn btn-primary mb-2">endbp</button>
                    <button id="medicine" name="medicine" class="btn btn-primary mb-2">endTask Медицина</button>
                    <button id="testing" name="testing" class="btn btn-primary mb-2">endTask Тестирование</button>
                </form>
                <div>
                    <button class="btn btn-primary mb-2" id="getphoto" name="getphoto">getphoto</button>
                    <div id="image" class="image"></div>
                </div>
            </div>

            <div id="result_form" class="result_form"></div>
        </div>
    </body>
</html>
